#!/usr/bin/bash

echo "Cloning Rinha Backend Repo"
#git clone https://github.com/zanfranceschi/rinha-de-backend-2023-q3.git ../rinha-de-backend-2023-q3

echo "Setting up the files"
cp rinha-primeira-fase.sh ../rinha-de-backend-2023-q3/rinha-primeira-fase.sh
cp rinha-final-live.sh ../rinha-de-backend-2023-q3/rinha-final-live.sh

mkdir -p ../rinha-de-backend-2023-q3/participantes/clojure-rocks
mkdir -p ../rinha-de-backend-2023-q3/resultados/primeira-fase/clojure-rocks
mkdir -p ../rinha-de-backend-2023-q3/resultados/final/clojure-rocks

cp nginx.conf ../rinha-de-backend-2023-q3/participantes/clojure-rocks/nginx.conf
cp docker-compose.yml ../rinha-de-backend-2023-q3/participantes/clojure-rocks/docker-compose.yml

cd ../rinha-de-backend-2023-q3

echo "Cleaning old results"
rm -rf resultados/primeira-fase/clojure-rocks
#rm -rf resultados/final/clojure-rocks

echo "Running tests"
./rinha-primeira-fase.sh
#./rinha-final-live.sh

# Go back to the repo
cd ../clj-rinha-backend
