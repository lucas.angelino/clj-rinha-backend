FROM clojure:temurin-17-lein-2.10.0-alpine as builder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY project.clj /usr/src/app/
RUN lein deps
COPY . /usr/src/app
RUN mv "$(lein uberjar | sed -n 's/^Created \(.*standalone\.jar\)/\1/p')" app-standalone.jar

FROM eclipse-temurin:17-alpine AS final
WORKDIR /service
COPY --from=builder /usr/src/app/app-standalone.jar /service/app-standalone.jar
#EXPOSE 3000
CMD ["java", "-jar", "app-standalone.jar"]
