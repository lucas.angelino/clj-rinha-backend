# clj-rinha-backend

An attempt to implement https://github.com/zanfranceschi/rinha-de-backend-2023-q3 chalenge using Clojure, Reitit, Ring, Postgres, Nginx, Docker, Redis & Cia.

- https://github.com/zanfranceschi/rinha-de-backend-2023-q3/blob/main/INSTRUCOES.md

## Usage

### Build Docker Image

```bash
docker build -t rinha-backend .
```

### How to run tests on Rinha Backend Repo


```bash
./run-local.sh
```

and spool the app logs

```bash
$ cd .../rinha-de-backend-2023-q3/participantes/clojure-rocks
$ docker compose logs -f > ~/app.log
```
