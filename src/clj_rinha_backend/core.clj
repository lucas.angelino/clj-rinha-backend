(ns clj-rinha-backend.core
  (:require [muuntaja.core :as m]
            #_[reitit.coercion.spec]
            [reitit.ring :as ring]
            #_[reitit.ring.coercion :as rrc]
            [reitit.ring.middleware.muuntaja :as middleware.muuntaja]
            [reitit.ring.middleware.parameters :as middleware.parameters]))

(def router (ring/router
              [["/pessoas" {:post {:handler (fn [req]
                                              #_(println "POST /pessoas")
                                              {:status 201
                                               :headers {"Location" (str "/pessoas/" (random-uuid))}
                                               :body   {}})}
                            :get  {:parameters {:query {:t string?}}
                                   :handler    (fn [req]
                                                 #_(println "GET /pessoas")
                                                 (if-let [t (get-in req [:query-params "t"])]
                                                   {:status 200
                                                    :body   {:msg (str (gensym "Teste") " t=" t)}}
                                                   {:status 400
                                                    :body   {:msg "Termo de busca não informado na request!"}}))}}]
               ["/pessoas/:id" {:get {:handler (fn [req]
                                                 #_(println "GET /pessoas/:id")
                                                 {:status 200
                                                  :body   {:id (get-in req [:path-params :id])}})}}]
               ["/contagem-pessoas" {:get {:handler (fn [req]
                                                      #_(println "GET /contagem-pessoas")
                                                      {:status 200
                                                       :body   (str (rand-int 9999))})}}]]
              ;; router data affecting all routes
              {:data {#_#_:coercion reitit.coercion.spec/coercion
                      :muuntaja   m/instance
                      :middleware [middleware.parameters/parameters-middleware
                                   #_rrc/coerce-request-middleware
                                   middleware.muuntaja/format-response-middleware
                                   #_rrc/coerce-response-middleware]}}))

(def app
  (ring/ring-handler
    router
    (ring/routes
      (ring/create-default-handler {:not-found (constantly {:status 404, :body "Not Found!"})}))))


(comment

  (require '[reitit.core :as r])
  (r/match-by-path router "/api/pessoas")
  (r/match-by-path router "/api/pessoas/1234")

  :end)
