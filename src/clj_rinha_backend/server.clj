(ns clj-rinha-backend.server
  (:require
    [clj-rinha-backend.core :as core]
    [ring.adapter.jetty :as jetty]
    #_[aleph.http :as aleph])
  (:gen-class))

(defn -main [& args]
  #_(aleph/start-server core/app
                      {:port (Integer/parseInt (or (System/getenv "PORT") "3000"))})
  (jetty/run-jetty core/app
                   {:port (Integer/parseInt (or (System/getenv "PORT") "3000"))}))

