(defproject clj-rinha-backend "0.1.0-SNAPSHOT"
  :description "Projeto Clojure para testes do Rinha Backend"
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [ring "1.11.0-RC1"]
                 #_[ring/ring-jetty-adapter "1.10.0"]
                 #_[ring/ring-jetty-adapter "1.11.0"]
                 #_[aleph "0.7.0-alpha1"]
                 [metosin/reitit "0.7.0-alpha5"]]
  #_#_:plugins [[lein-ring "0.12.6"]]
  :main clj-rinha-backend.server
  #_#_:ring {:handler clj-rinha-backend.core/app}
  :profiles {:dev {:source-paths ["dev"]}
             :uberjar {:aot :all}}
  :repl-options {:init-ns clj-rinha-backend.core})
